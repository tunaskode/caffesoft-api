<?php
    require_once('../config/connect.php');

    if($_SERVER['REQUEST_METHOD']=='POST') {
        $response = array();
    
        $kode_pesanan = $_POST['kode_pesanan'];
    
        $sql = "DELETE FROM tbl_pesanan WHERE kode_pesanan = '$kode_pesanan'";

        if(mysqli_query($con,$sql)) {
            $response["value"] = 1;
            $response["message"] = "Data $kode_pesanan dengan id  berhasil dihapus";
            echo json_encode($response);
        } else {
            $response["value"] = 0;
            $response["message"] = "Maaf! Gagal dihapus datanya!";
            echo json_encode($response);
        }
    
        mysqli_close($con);
    }  else {
        $response["msg"] = "Bro, parameter untuk deletenya belum ada";
        $response["value"] = 0;
        $response["message"] = "Maaf! Coba lagi!";
        echo json_encode($response);
    }

?>