<?php

    include '../config/connect.php';

    $sql=mysqli_query($con,"SELECT * FROM tbl_pesanan");

    if (isset($_GET["kode_pesanan"]) || isset($_GET["kode_pemesan"]) || isset($_GET["kode_waitress"])) {
        $kode_pesanan = $_GET["kode_pesanan"];
        $kode_pemesan = $_GET["kode_pemesan"];
        $kode_waitress = $_GET["kode_waitress"];

        $sql = mysqli_query($con,"SELECT tbl_pesanan.id_pesanan, tbl_pesanan.kode_pesanan, tbl_pesanan.tanggal_pesanan, tbl_pemesan.kode_pemesan, tbl_pemesan.nama_pemesan, tbl_staff.kode_waitress, tbl_staff.nama_waitress, tbl_meja.nama_meja, tbl_pesanan.metode_bayar FROM tbl_pesanan JOIN tbl_pemesan ON tbl_pesanan.kode_pemesan=tbl_pemesan.kode_pemesan JOIN tbl_staff ON tbl_pesanan.kode_waitress=tbl_staff.kode_waitress JOIN tbl_meja ON tbl_pesanan.kode_meja=tbl_meja.kode_meja WHERE tbl_pesanan.kode_pesanan = '$kode_pesanan' AND tbl_pesanan.kode_pemesan = '$kode_pemesan' AND tbl_pesanan.kode_waitress = '$kode_waitress'");
    }

    $response = array();
    $cek = mysqli_num_rows($sql);
    if ($cek > 0) {
        $response["detail_pesanan"]=array();

        while ($row=mysqli_fetch_array($sql)){

            $data=array();
            $data["id_pesanan"]=$row["id_pesanan"];
            $data["kode_pesanan"]=$row["kode_pesanan"];
            $data["tanggal_pesanan"]=$row["tanggal_pesanan"];
            $data["kode_pemesan"]=$row["kode_pemesan"];
            $data["nama_pemesan"]=$row["nama_pemesan"];
            $data["kode_waitress"]=$row["kode_waitress"];
            $data["nama_waitress"]=$row["nama_waitress"];
            $data["nama_meja"]=$row["nama_meja"];
            $data["metode_bayar"]=$row["metode_bayar"];
            
            $response["msg"]="Data pesanan found.";
            $response["code"]=200;
            $response["status"]=true;    
            array_push($response["detail_pesanan"], $data);
        }

        echo json_encode($response);

    }else{
        $response["msg"]="Data pesanan not found.";
        $response["code"]=404;
        $response["status"]=false; 
        echo json_encode($response);
    } 

?>